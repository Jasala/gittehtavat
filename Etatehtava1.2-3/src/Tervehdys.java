
import java.time.LocalTime;

public class Tervehdys {

    public static void main(String[] args) {
        
        LocalTime time = LocalTime.now();
        LocalTime noon = LocalTime.parse("12:00:00");
        
        if(time.isBefore(noon))
        {
            System.out.println("Hyvää huomenta");
        }
        else
        {
            System.out.println("Hyvää iltaa");
        }
}
}
