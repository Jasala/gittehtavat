package Entity;

public class Matriisi
{

    private int ekaLuku;
    private int rivi;
    private int kolumni;
    private int ekaSolu;
    
    public void setRivi(int rivi)
    {
        this.rivi = rivi;
    }
    public int getRivi()
    {
        return rivi;
    }
    public void setKolumni(int kolumni)
    {
        this.kolumni = kolumni;
    }
    public int getKolumni()
    {
        return kolumni;
    }
    public void setEkaLuku(int EkaLuku)
    {
        this.ekaLuku = EkaLuku;
    }
    public int getEkaLuku()
    {
        return ekaLuku;
    }
    public void setEkaSolu(int EkaSolu)
    {
        this.ekaSolu = EkaSolu;
    }  
    public int getEkaSolu()
    {
        return ekaSolu;
    }
}
