package Paaohjelma;

import Entity.Matriisi;
import Service.MatriisiLaskin;

public class Etatehtava2
{
    public static void main(String[] args) 
    {
        MatriisiLaskin matriisiLaskuri = new MatriisiLaskin();
        
        Matriisi matriisi1 = new Matriisi();
        matriisi1.setEkaLuku(1);
        matriisi1.setRivi(4);
        matriisi1.setKolumni(1);
        matriisiLaskuri.luoMatriisi1(matriisi1);
        
        System.out.println("");
        
        Matriisi matriisi2 = new Matriisi();
        matriisi2.setEkaSolu(1);
        matriisiLaskuri.luoMatriisi2(matriisi2);
        
        System.out.println("");
        
        matriisiLaskuri.matrixSum();
        
        System.out.println("");
        
        matriisiLaskuri.matrixMult();
    }
    
}