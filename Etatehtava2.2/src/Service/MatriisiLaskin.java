package Service;
import Entity.Matriisi;
public class MatriisiLaskin 
{
    final int[][] matriisi1 = new int[5][5];
    final int[][] matriisi2 = new int[5][5];
    private int[][] summaMatriisi = new int[5][5];
    private int[][] tuloMatriisi = new int[5][5];
    
    public void luoMatriisi1(Matriisi tietoja)
    {
        for (int x=0; x<matriisi1.length; ++x)
        {
            for (int y=0; y<matriisi1[x].length; ++y)
            {
                if ((x == tietoja.getRivi()) && (y == tietoja.getKolumni()))
                {
                    matriisi1[x][y] = matriisi1[x][y] + tietoja.getEkaLuku();
                    System.out.printf("%4d", matriisi1[x][y]);
                }
                else
                {
                    matriisi1[x][y] = matriisi1[x][y] + tietoja.getEkaLuku() + 5;
                    System.out.printf("%4d", matriisi1[x][y]);
                }
                    
            }
            System.out.println();
        }

    }
    public void luoMatriisi2(Matriisi tietoja)
    {
        for (int x=0; x<matriisi2.length; ++x)
        {
            for (int y=0; y<matriisi2[x].length; ++y)
            {
                if ((x == 0) && (y == 0))
                {
                    matriisi2[x][y] = tietoja.getEkaSolu();
                    System.out.printf("%4d", matriisi2[x][y]);
                }
                else if ((x > 0) && (y == 0))
                {
                    int oldx = x - 1;
                    int oldy = y + 4;
                    matriisi2[x][y] = matriisi2[oldx][oldy] + matriisi2[oldx][oldy];
                    System.out.printf("%4d   ", matriisi2[x][y]);
                }
                else
                {
                    int oldy = y - 1;
                    matriisi2[x][y] = matriisi2[x][oldy] + matriisi2[x][oldy];
                    System.out.printf("%4d   ", matriisi2[x][y]);
                }
            }
            System.out.println();
        }

    }
    public void matrixSum()
    {
        if (matriisi1.length == matriisi2.length)
        {
            for (int x=0; x<summaMatriisi.length; ++x)
            {
                for (int y=0; y<summaMatriisi[x].length; ++y)
                {
                    summaMatriisi[x][y] = matriisi1[x][y] + matriisi2[x][y];
                    System.out.printf("%4d   ", summaMatriisi[x][y]);
                }  
                System.out.println();
            }
        }

    }
    public void matrixMult()
    {
        if (matriisi1.length == matriisi2.length)
        {
            for (int x=0; x<tuloMatriisi.length; ++x)
            { 
                for (int y=0; y<tuloMatriisi[x].length; ++y)
                {
                    tuloMatriisi[x][y] = matriisi1[x][y] * matriisi2[x][y];
                    System.out.printf("%4d   ", tuloMatriisi[x][y]);
                } 
            System.out.println();
            }
        }    
    }
}